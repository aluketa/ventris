package com.nine.ventris.config;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.db.DataSourceFactory;

public class PersistenceConfiguration {
    private MongoConfiguration mongoConfiguration;
    private DataSourceFactory sqlDatabase;

    @JsonProperty("mongoDb")
    public MongoConfiguration getMongoConfiguration() {
        return mongoConfiguration;
    }

    @JsonProperty("mongoDb")
    public void setMongoConfiguration(MongoConfiguration mongoConfiguration) {
        this.mongoConfiguration = mongoConfiguration;
    }

    public DataSourceFactory getSqlDatabase() {
        return sqlDatabase;
    }

    public void setSqlDatabase(DataSourceFactory sqlDatabase) {
        this.sqlDatabase = sqlDatabase;
    }
}
