package com.nine.ventris.config;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;

public class VentrisConfiguration extends Configuration {
    private TwitterConfiguration twitterConfiguration = new TwitterConfiguration();
    private ClassificationConfiguration classificationConfiguration = new ClassificationConfiguration();
    private PersistenceConfiguration persistenceConfiguration = new PersistenceConfiguration();
    private EmailerConfiguration emailerConfiguration = new EmailerConfiguration();

    @JsonProperty("twitter")
    public TwitterConfiguration getTwitterConfiguration() {
        return twitterConfiguration;
    }

    @JsonProperty("twitter")
    public void setTwitterConfiguration(TwitterConfiguration twitterConfiguration) {
        this.twitterConfiguration = twitterConfiguration;
    }

    @JsonProperty("classification")
    public ClassificationConfiguration getClassificationConfiguration() {
        return classificationConfiguration;
    }

    @JsonProperty("classification")
    public void setClassificationConfiguration(ClassificationConfiguration classificationConfiguration) {
        this.classificationConfiguration = classificationConfiguration;
    }

    @JsonProperty("persistence")
    public PersistenceConfiguration getPersistenceConfiguration() {
        return persistenceConfiguration;
    }

    @JsonProperty("persistence")
    public void setPersistenceConfiguration(PersistenceConfiguration persistenceConfiguration) {
        this.persistenceConfiguration = persistenceConfiguration;
    }

    @JsonProperty("emailer")
    public EmailerConfiguration getEmailerConfiguration() {
        return emailerConfiguration;
    }

    @JsonProperty("emailer")
    public void setEmailerConfiguration(EmailerConfiguration emailerConfiguration) {
        this.emailerConfiguration = emailerConfiguration;
    }
}
