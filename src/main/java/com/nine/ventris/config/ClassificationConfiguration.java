package com.nine.ventris.config;

import org.joda.time.LocalDate;

public class ClassificationConfiguration {
    private boolean migrateDb;
    private LocalDate startDate;
    private LocalDate endDate;

    public boolean isMigrateDb() {
        return migrateDb;
    }

    public void setMigrateDb(boolean migrateDb) {
        this.migrateDb = migrateDb;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }
}
