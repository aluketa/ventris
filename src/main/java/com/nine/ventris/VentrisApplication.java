package com.nine.ventris;

import com.nine.ventris.command.ClassifyTweets;
import com.nine.ventris.command.CollectTweets;
import com.nine.ventris.config.VentrisConfiguration;
import com.nine.ventris.resource.VersionResource;
import io.dropwizard.Application;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class VentrisApplication extends Application<VentrisConfiguration> {
    @Override
    public void initialize(Bootstrap<VentrisConfiguration> bootstrap) {
        bootstrap.addBundle(new AssetsBundle("/assets", "/", "index.html"));
        bootstrap.addCommand(new CollectTweets());
        bootstrap.addCommand(new ClassifyTweets());
    }

    @Override
    public void run(VentrisConfiguration ventrisConfiguration, Environment environment) throws Exception {
        environment.jersey().register(VersionResource.class);
        environment.jersey().setUrlPattern("/rest/*");
    }

    public static void main(String... args) throws Exception {
        new VentrisApplication().run(args);
    }
}
