package com.nine.ventris.twitter

import java.nio.charset.Charset
import java.util.Base64

import com.fasterxml.jackson.annotation.JsonProperty
import com.nine.ventris.util.Json
import org.apache.http.client.HttpClient
import org.apache.http.client.methods.{HttpGet, HttpPost, HttpUriRequest}
import org.apache.http.entity.StringEntity
import org.apache.http.impl.client.HttpClientBuilder

object TwitterApi {

  def authenticate(key: String, secret: String, client: HttpClient = HttpClientBuilder.create().build()): TwitterApi = {
    val response = client.execute(buildAuthenticationRequest(key, secret))
    new TwitterApiImpl(Json.fromEntity[AuthenticationToken](response.getEntity), client)
  }

  private def buildAuthenticationRequest(key: String, secret: String): HttpUriRequest = {
    val authorisation = Base64.getEncoder.encodeToString((key + ":" + secret).getBytes)

    val request = new HttpPost("https://api.twitter.com/oauth2/token")
    request.setHeader("Authorization", s"Basic $authorisation")
    request.setHeader("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8")
    request.setEntity(new StringEntity("grant_type=client_credentials", Charset.forName("UTF8")))

    request
  }
}

trait TwitterApi {
  def getTweets(user: String, count: Option[Int] = None, sinceId: Option[Long] = None, maxId: Option[Long] = None, excludeReplies: Boolean = true, includeRetweets: Boolean = false, trimUser: Boolean = true): Seq[Tweet]
}

class TwitterApiImpl(token: AuthenticationToken, client: HttpClient) extends TwitterApi {
  private val GetStatusesUserTimeLine = "https://api.twitter.com/1.1/statuses/user_timeline.json"

  override def getTweets(user: String, count: Option[Int] = None, sinceId: Option[Long] = None, maxId: Option[Long] = None, excludeReplies: Boolean = true, includeRetweets: Boolean = false, trimUser: Boolean = true): Seq[Tweet] = {
    val optionMap = Map(
      "screen_name" -> user,
      "exclude_replies" -> excludeReplies,
      "include_rts" -> includeRetweets,
      "trim_user" -> trimUser
    ) ++
      count.map(c => ("count", c)).toMap ++
      maxId.map(i => ("max_id", i)).toMap ++
      sinceId.map(i => ("since_id", i)).toMap

    val optionString = optionMap.map { case (key, value) => s"$key=$value" }.mkString("&")

    val request = new HttpGet(s"$GetStatusesUserTimeLine?$optionString")
    request.setHeader("Authorization", s"${token.tokenType} ${token.accessToken}")

    val response = client.execute(request)
    Json.fromEntity[Seq[Map[String, Object]]](response.getEntity) map (mt => new Tweet(mt))
  }
}

case class AuthenticationToken(@JsonProperty("token_type") tokenType: String, @JsonProperty("access_token") accessToken: String)
