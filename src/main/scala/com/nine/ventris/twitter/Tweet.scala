package com.nine.ventris.twitter

import org.joda.time.LocalDateTime
import org.joda.time.format.DateTimeFormat

object Tweet {
  def apply(id: Long): Tweet = new Tweet(Map("id" -> id))
  def apply(id: Long, text: String): Tweet = new Tweet(Map("id" -> id, "text" -> text))
  def apply(id: Long, createdAt: LocalDateTime): Tweet = new Tweet(Map("id" -> id, "created_at" -> createdAt.toString("EEE MMM dd HH:mm:ss +0000 yyyy")))
}

class Tweet(val properties: Map[String, Any]) {
  def property(key: String): Any = properties(key)

  def id: Long = property("id").asInstanceOf[Long]

  def createdAt: LocalDateTime = properties.get("created_at") match {
    case Some(ca: String) => LocalDateTime.parse(ca, DateTimeFormat.forPattern("EEE MMM dd HH:mm:ss Z yyyy"))
    case _ => new LocalDateTime(0L)
  }

  def text: String = property("text").toString

  override def hashCode(): Int = property("id").hashCode()

  override def equals(other: Any): Boolean = other match {
    case that: Tweet => properties("id") == that.properties("id")
    case _ => false
  }

  override def toString: String = properties.toString()
}
