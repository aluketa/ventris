package com.nine.ventris.classification

object Classification extends Enumeration {
  type Classification = Value
  val TrainFailure, ShortFormation = Value
}
