package com.nine.ventris.classification

import com.nine.ventris.classification.Classification.Classification
import com.nine.ventris.twitter.Tweet

object Classifier {
  def classify(tweet: Tweet): Seq[Classification] = {
    if (tweet.text contains "train failure")
      Seq(Classification.TrainFailure)
    else if (tweet.text contains "formed of 8")
      Seq(Classification.ShortFormation)
    else
      Seq()
  }
}

