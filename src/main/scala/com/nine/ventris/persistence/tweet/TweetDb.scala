package com.nine.ventris.persistence.tweet

import com.nine.ventris.twitter.Tweet
import org.joda.time.LocalDate

trait TweetDb {
  def minId: Option[Long] = None

  def maxId: Option[Long] = None

  def saveTweet(tweet: Tweet)

  def saveAllTweets(tweets: Seq[Tweet]) = tweets foreach saveTweet

  def getTweets(startDate: LocalDate, endDate: LocalDate): Seq[Tweet]

  def recordRunInfo(runInfo: RunInfo): Unit

  def lastRunInfo: Option[RunInfo]
}