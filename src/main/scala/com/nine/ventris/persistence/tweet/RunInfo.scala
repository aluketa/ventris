package com.nine.ventris.persistence.tweet

import com.mongodb.DBObject
import com.mongodb.casbah.commons.MongoDBObject
import org.bson.types.ObjectId
import org.joda.time.{DateTime, LocalDateTime}

object RunInfo {
  def toBson(runInfo: RunInfo): DBObject =
    MongoDBObject(
      "startTime" -> runInfo.startTime,
      "endTime" -> runInfo.endTime,
      "sinceId" -> runInfo.sinceId,
      "maxId" -> runInfo.maxId,
      "_id" -> runInfo.id
    )

  def fromBson(obj: DBObject): RunInfo =
    RunInfo(
      obj.get("startTime").asInstanceOf[DateTime].toLocalDateTime,
      obj.get("endTime").asInstanceOf[DateTime].toLocalDateTime,
      obj.get("sinceId").asInstanceOf[Long],
      obj.get("maxId").asInstanceOf[Long]
    )
}

case class RunInfo(startTime: LocalDateTime, endTime: LocalDateTime, sinceId: Long, maxId: Long, id: ObjectId = new ObjectId())
