package com.nine.ventris.persistence.tweet

import java.util.{Map => JMap}

import com.mongodb.MongoException
import com.mongodb.casbah.Imports._
import com.mongodb.casbah.commons.conversions.scala.RegisterJodaTimeConversionHelpers
import com.nine.ventris.twitter.Tweet
import org.joda.time.LocalDate
import org.slf4j.LoggerFactory

import scala.collection.JavaConversions._

class MongoTweetDb(host: String, port: Int) extends TweetDb {
  private val logger = LoggerFactory.getLogger(classOf[MongoTweetDb])

  private val db = MongoClient(host, port)("ventris")
  RegisterJodaTimeConversionHelpers()

  override def minId: Option[Long] = {
    db("tweets").map(x => x.get("id").asInstanceOf[Long]) match {
      case Nil => None
      case ids: Seq[Long] => Some(ids.min)
    }
  }

  override def maxId: Option[Long] = {
    db("tweets").map(x => x.get("id").asInstanceOf[Long]) match {
      case Nil => None
      case ids: Seq[Long] => Some(ids.max)
    }
  }

  override def saveTweet(tweet: Tweet): Unit = {
    val tweetProperties = tweet.properties + ("_id" -> tweet.id) + ("createdAt" -> tweet.createdAt.toDate)
    try {
      db("tweets").insert(MongoDBObject(tweetProperties.toSeq: _*))
    } catch {
      case e: MongoException.DuplicateKey => logger.warn("Duplicate key, record already exists")
    }
  }

  override def getTweets(startDate: LocalDate, endDate: LocalDate): Seq[Tweet] = {
    val tweetCursor = db("tweets").find("createdAt" $gte startDate.toDate $lt endDate.toDate)
    tweetCursor.map(obj => new Tweet(asMap(obj.toMap))).toSeq
  }

  override def recordRunInfo(runInfo: RunInfo): Unit = db("runDetails").insert(RunInfo.toBson(runInfo))

  override def lastRunInfo: Option[RunInfo] =
    db("runDetails").lastOption.map(obj => RunInfo.fromBson(obj))

  private def asMap(javaMap: JMap[_,_]): Map[String, Any] =
    javaMap.entrySet().map(e => (e.getKey.toString, e.getValue)).toMap
}
