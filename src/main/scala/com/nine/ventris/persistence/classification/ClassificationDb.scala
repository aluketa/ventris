package com.nine.ventris.persistence.classification

import com.nine.ventris.classification.Classification.Classification
import com.nine.ventris.twitter.Tweet
import org.joda.time.LocalDate

trait ClassificationDb {
  def saveClassifications(tweet: Tweet, classifications: Seq[Classification])

  def deleteClassifications(startDate: LocalDate, endDate: LocalDate)
}
