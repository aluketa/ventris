package com.nine.ventris.persistence.classification

import java.util.Date

import com.nine.ventris.classification.Classification.Classification
import com.nine.ventris.config.VentrisConfiguration
import com.nine.ventris.twitter.Tweet
import io.dropwizard.jdbi.DBIFactory
import io.dropwizard.setup.{Bootstrap, Environment}
import org.joda.time.LocalDate
import org.skife.jdbi.v2.DBI
import org.skife.jdbi.v2.sqlobject.{Bind, SqlUpdate}

class SqlClassificationDb(bootstrap: Bootstrap[VentrisConfiguration], config: VentrisConfiguration) extends ClassificationDb {
  private val dao = configureJdbi(bootstrap, config).onDemand(classOf[ClassificationDAO])

  override def saveClassifications(tweet: Tweet, classifications: Seq[Classification]): Unit = {
    dao.saveTweet(tweet.id, tweet.text, tweet.createdAt.toDate)
    classifications foreach(classification => dao.saveClassification(tweet.id, classification.toString))
  }

  override def deleteClassifications(startDate: LocalDate, endDate: LocalDate): Unit = {
    dao.deleteClassifications(startDate, endDate)
    dao.deleteTweets(startDate, endDate)
  }

  private def configureJdbi(bootstrap: Bootstrap[VentrisConfiguration], config: VentrisConfiguration): DBI =
    new DBIFactory().build(environment(bootstrap), config.getPersistenceConfiguration.getSqlDatabase, "SQL Database")

  private def environment(bootstrap: Bootstrap[VentrisConfiguration]): Environment =
    new Environment(bootstrap.getApplication.getName,
      bootstrap.getObjectMapper,
      bootstrap.getValidatorFactory.getValidator,
      bootstrap.getMetricRegistry,
      bootstrap.getClassLoader)
}

private trait ClassificationDAO {
  @SqlUpdate("delete from tweets where createdAt between :startDate and :endDate")
  def deleteTweets(@Bind("startDate") startDate: LocalDate, @Bind("endDate") endDate: LocalDate)

  @SqlUpdate("delete from tweet_classifications where id in (select id from tweets where createdAt between :startDate and :endDate)")
  def deleteClassifications(@Bind("startDate") startDate: LocalDate, @Bind("endDate") endDate: LocalDate)

  @SqlUpdate("insert into tweets (id, text, createdAt) values (:id, :text, :createdAt)")
  def saveTweet(@Bind("id") id: Long, @Bind("text") text: String, @Bind("createdAt") createdAt: Date)

  @SqlUpdate("insert into tweet_classifications (id, classification) values (:id, :classification)")
  def saveClassification(@Bind("id") id: Long, @Bind("classification") classification: String)
}