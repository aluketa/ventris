package com.nine.ventris.command

import com.nine.ventris.classification.Classifier.classify
import com.nine.ventris.config.VentrisConfiguration
import com.nine.ventris.persistence.classification.{SqlClassificationDb, ClassificationDb}
import com.nine.ventris.persistence.tweet.{MongoTweetDb, TweetDb}
import com.nine.ventris.util.Migrator
import io.dropwizard.cli.ConfiguredCommand
import io.dropwizard.setup.Bootstrap
import net.sourceforge.argparse4j.inf.Namespace
import org.slf4j.LoggerFactory.getLogger

class ClassifyTweets
  extends ConfiguredCommand[VentrisConfiguration](
    "classifyTweets",
    "Classifies tweets based on their content and stores the classification in the configured persistence layer") {
  private val logger = getLogger(getClass)
  protected val migrator = new Migrator

  override def run(bootstrap: Bootstrap[VentrisConfiguration], namespace: Namespace, config: VentrisConfiguration): Unit = {
    val startDate = config.getClassificationConfiguration.getStartDate
    val endDate = config.getClassificationConfiguration.getEndDate
    logger.info(s"Classifying tweets between $startDate and $endDate")

    if (config.getClassificationConfiguration.isMigrateDb) {
      logger.info("Migrating database changes")
      migrator.migrate(bootstrap, config)
    }

    val tweetDb = configureTweetDb(config)
    val classificationDb = configureClassificationDb(bootstrap, config)

    val tweets = tweetDb.getTweets(config.getClassificationConfiguration.getStartDate, config.getClassificationConfiguration.getEndDate)
    logger.info(s"Retrieved ${tweets.size} for classification")

    logger.info("Deleting previous data.")
    classificationDb.deleteClassifications(startDate, endDate)
    logger.info("Commencing classification")
    tweets.foreach(t => classificationDb.saveClassifications(t, classify(t)))

    logger.info("Classification completed successfully")
  }

  protected def configureTweetDb(config: VentrisConfiguration): TweetDb = {
    val host = config.getPersistenceConfiguration.getMongoConfiguration.getMongoHost
    val port = config.getPersistenceConfiguration.getMongoConfiguration.getMongoPort

    new MongoTweetDb(host, port)
  }

  protected def configureClassificationDb(bootstrap: Bootstrap[VentrisConfiguration], config: VentrisConfiguration): ClassificationDb =
    new SqlClassificationDb(bootstrap, config)
}
