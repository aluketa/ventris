package com.nine.ventris.command

import java.util.concurrent.atomic.AtomicInteger

import com.google.common.util.concurrent.RateLimiter
import com.nine.ventris._
import com.nine.ventris.config.VentrisConfiguration
import com.nine.ventris.persistence.tweet.{RunInfo, MongoTweetDb, TweetDb}
import com.nine.ventris.twitter.TwitterApi
import com.nine.ventris.util.{DefaultEmailer, Emailer}
import io.dropwizard.cli.ConfiguredCommand
import io.dropwizard.setup.Bootstrap
import net.sourceforge.argparse4j.inf.Namespace
import org.joda.time.LocalDateTime
import org.slf4j.LoggerFactory.getLogger

class CollectTweets
  extends ConfiguredCommand[VentrisConfiguration] (
    "collectTweets",
    "Downloads tweets for the configured account up to the configured date") {

  private val logger = getLogger(getClass)
  protected val rateLimiter = RateLimiter.create((280.0 * 4.0) / (60.0 * 60.0))
  protected def now: LocalDateTime = LocalDateTime.now()

  override def run(bootstrap: Bootstrap[VentrisConfiguration], namespace: Namespace, config: VentrisConfiguration): Unit = {
    val emailer = configureEmailer(config)
    val startTime = now

    try {
      val tweetDb = configureTweetDb(config)
      val twitterApi = configureTwitterApi(config)

      val twitterUser = config.getTwitterConfiguration.getTargetUserName
      val currentCount = new AtomicInteger(0)

      var working = true
      val sinceId: Option[Long] = tweetDb.lastRunInfo.map(_.maxId + 1)
      var nextId: Option[Long] = None
      logger.info(s"Retrieving tweets for account: ${config.getTwitterConfiguration.getTargetUserName} since ID $sinceId")

      while (working) {
        val tweets = twitterApi.getTweets(twitterUser, maxId = nextId, sinceId = sinceId)
        nextId = tweets.map(_.id).reduceLeftOption(_ min _).map(_ - 1)
        logger.info(s"Retrieved ${tweets.size} tweets, current count: ${currentCount.addAndGet(tweets.size)}")
        tweetDb.saveAllTweets(tweets)
        working = tweets.nonEmpty
        //Due to limitations with the twitter api returning re-tweets when specifying a count, we are applying the rate limit
        //after retrieval and setting a conservative limit in order to prevent over-runs.
        if (tweets.nonEmpty) rateLimiter.acquire(tweets.size)
      }

      logger.info("Tweet retrieval completed successfully")
      emailer.sendMail("Tweet retrieval completed successfully", s"CollectTweets command successfully processed ${currentCount.get} tweets.")
      tweetDb.recordRunInfo(RunInfo(startTime, now, sinceId.getOrElse(0), tweetDb.maxId.getOrElse(0)))
    } catch {
      case e: Exception =>
        logger.error("Tweet retrieval failed", e)
        emailer.sendMail("Tweet retrieval failed", e.asString)
    }
  }

  protected def configureTweetDb(config: VentrisConfiguration): TweetDb = {
    val host = config.getPersistenceConfiguration.getMongoConfiguration.getMongoHost
    val port = config.getPersistenceConfiguration.getMongoConfiguration.getMongoPort

    new MongoTweetDb(host, port)
  }

  protected def configureTwitterApi(config: VentrisConfiguration): TwitterApi = {
    val key = config.getTwitterConfiguration.getKey
    val secret = config.getTwitterConfiguration.getSecret

    TwitterApi.authenticate(key, secret)
  }

  protected def configureEmailer(config: VentrisConfiguration): Emailer =
    new DefaultEmailer(config.getEmailerConfiguration)
}
