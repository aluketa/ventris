package com.nine.ventris.resource

import javax.ws.rs.core.MediaType
import javax.ws.rs.{Produces, GET, Path}

@Path("/version")
class VersionResource {
  @GET
  @Produces(Array(MediaType.TEXT_PLAIN))
  def version = "1.0"
}
