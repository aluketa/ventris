package com.nine

import java.io.{StringWriter, PrintWriter}

package object ventris {
  implicit class RichThrowable(t: Throwable) {
    def asString = {
      val sw = new StringWriter()
      val pw = new PrintWriter(sw)
      t.printStackTrace(pw)

      val result = sw.toString
      pw.close()
      sw.close()

      result
    }
  }
}
