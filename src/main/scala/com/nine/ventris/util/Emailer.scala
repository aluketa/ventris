package com.nine.ventris.util

import java.util.{Date, Properties}
import javax.mail.internet.{InternetAddress, MimeMessage}
import javax.mail.{Message, Session}

import com.nine.ventris.config.EmailerConfiguration

trait Emailer {
  def sendMail(recipient: String, subject: String, body: String)
  def sendMail(subject: String, body: String)
}

class DefaultEmailer(config: EmailerConfiguration) extends Emailer {

  private val hostname = config.getHostname
  private val from = config.getFromAddress
  private val username = config.getUsername
  private val password = config.getPassword
  private val notificationAddress = config.getNotificationAddress

  private val mailProperties = {
    val props = new Properties
    props.setProperty("mail.smtp.auth", "true")
    props.setProperty("mail.smtp.sasl.enable", "true")

    props
  }

  override def sendMail(subject: String, body: String) {
    sendMail(notificationAddress, subject, body)
  }

  override def sendMail(recipient: String, subject: String, body: String) {
    val session = Session.getInstance(mailProperties)
    val message = createMessage(session, recipient, subject, body)

    sendMessage(session, message)
  }

  private def createMessage(session: Session, recipient: String, subject: String, body: String): Message = {
    val message = new MimeMessage(session)
    message.setFrom(new InternetAddress(from))
    message.setRecipients(Message.RecipientType.TO, recipient)
    message.setSubject(subject)
    message.setText(body)
    message.setSentDate(new Date)

    message
  }

  private def sendMessage(session: Session, message: Message) {
    val transport = session.getTransport("smtp")
    transport.connect(hostname, username, password)
    transport.sendMessage(message, message.getAllRecipients)
    transport.close()
  }
}