package com.nine.ventris.util

import java.sql.Connection

import com.nine.ventris.config.VentrisConfiguration
import io.dropwizard.db.ManagedDataSource
import io.dropwizard.setup.Bootstrap
import liquibase.Liquibase
import liquibase.database.jvm.JdbcConnection
import liquibase.database.{Database, DatabaseFactory}
import liquibase.resource.ClassLoaderResourceAccessor

class Migrator {
  def migrate(bootstrap: Bootstrap[VentrisConfiguration], configuration: VentrisConfiguration): Unit = {
    val managedDataSource: ManagedDataSource = configuration.getPersistenceConfiguration.getSqlDatabase.build(bootstrap.getMetricRegistry, "SQL Database")
    val connection: Connection = managedDataSource.getConnection
    val database: Database = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(new JdbcConnection(connection))
    val liquibase = new Liquibase("migrations.xml", new ClassLoaderResourceAccessor(), database)

    liquibase.update("")
  }
}
