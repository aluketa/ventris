package com.nine.ventris.util

import java.io.ByteArrayOutputStream

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import org.apache.http.HttpEntity

import scala.reflect.ClassTag

object Json {
  private val mapper = {
    val om = new ObjectMapper
    om.registerModule(DefaultScalaModule)
    om
  }

  def fromJson[T](json: String)(implicit m: ClassTag[T]): T = mapper.readValue(json, m.runtimeClass).asInstanceOf[T]

  def toJson(obj: Any): String =  mapper.writeValueAsString(obj)

  def fromEntity[T](entity: HttpEntity)(implicit m: ClassTag[T]): T = {
    val os = new ByteArrayOutputStream
    try {
      entity.writeTo(os)
      fromJson(os.toString)
    } finally {
      os.close()
    }
  }
}
