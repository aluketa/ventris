package com.nine.ventris.util

import java.nio.charset.Charset

import org.apache.http.entity.StringEntity
import org.specs2.mutable.Specification

class JsonSpec extends Specification {
  "The JSON helper object" should {
    "Convert a string into a JSON and back again" >> {
      val test = "Testing 123"
      val json = Json.toJson(test)

      json must beEqualTo("\"Testing 123\"")
      Json.fromJson[String](json) must beEqualTo(test)
    }


    "Convert an integer to JSON and back again" >> {
      val test = 2015
      val json = Json.toJson(test)

      json must beEqualTo("2015")
      Json.fromJson[Int](json) must beEqualTo(test)
    }

    "Convert a simple case class to JSON and back again" >> {
      val test = SimpleObject("Test Key", "Test Value")
      val json = Json.toJson(test)

      json must beEqualTo("{\"key\":\"Test Key\",\"value\":\"Test Value\"}")
      Json.fromJson[SimpleObject](json) must beEqualTo(test)
    }

    "Retrieve an Object from an Http Response Entity" >> {
      val entity = new StringEntity("{\"key\":\"Test Key\",\"value\":\"Test Value\"}", Charset.forName("UTF8"))

      Json.fromEntity[SimpleObject](entity) must beEqualTo(SimpleObject("Test Key", "Test Value"))
    }
  }
}

case class SimpleObject(key: String, value: String)
