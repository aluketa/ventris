package com.nine.ventris.twitter

import org.joda.time.LocalDateTime
import org.joda.time.format.DateTimeFormat
import org.specs2.mutable.Specification

class TweetSpec extends Specification {
  "The Tweet class" should {
    "return the correct createdAt value for a given tweet" >> {
      val tweet = new Tweet(Map("created_at" -> "Mon Dec 29 17:59:57 +0000 2014"))
      tweet.createdAt must beEqualTo(LocalDateTime.parse("2014-12-29 17:59:57", DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")))
    }

    "return the correct id value for a given tweet" >> {
      val tweet = new Tweet(Map("id" -> 12345L))
      tweet.id must beEqualTo(12345L)
    }

    "return the correct text value for a given tweet" >> {
      val tweet = new Tweet(Map("text" -> "Test text"))
      tweet.text must beEqualTo("Test text")
    }
  }
}
