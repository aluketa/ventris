package com.nine.ventris.twitter

import java.util.Base64

import org.apache.http.entity.StringEntity
import org.apache.http.message.{BasicHttpResponse, BasicStatusLine}
import org.apache.http.{HttpResponse, ProtocolVersion}
import org.specs2.mutable.Specification

class TwitterApiSpec extends Specification {
  private val authenticationToken = AuthenticationToken("bearer", "ABCDEF")

  "The twitter API" should {
    "create an authorised instance of the Twitter api" >> {
      val client = new MockHttpClient("Test Key", "Test Secret", request => {
        request.getFirstHeader("Content-Type").getValue must beEqualTo("application/x-www-form-urlencoded;charset=UTF-8")

        val authorisation = request.getFirstHeader("Authorization")
        authorisation.getValue must startWith("Basic ")

        val decoded = new String(Base64.getDecoder.decode(authorisation.getValue.substring("Basic ".length)))
        decoded must beEqualTo("Test Key:Test Secret")

        respondWith("{\"token_type\": \"bearer\",\"access_token\":\"AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA%2FAAAAAAAAAAAAAAAAAAAA%3DAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\"}")
      })

      val twitterApi = TwitterApi.authenticate("Test Key", "Test Secret", client)
      twitterApi mustNotEqual null
    }

    "set the correct authorization headers when getting tweets" >> {
      val client = new MockHttpClient (request => {
        request.getFirstHeader("Authorization").getValue must beEqualTo("bearer ABCDEF")
        respondWith("[]")
      })

      val tweetData = new TwitterApiImpl(authenticationToken, client)
      tweetData.getTweets("testUser") must haveSize(0)
    }

    "set the correct REST url when getting tweets" >> {
      val client = new MockHttpClient (request => {
        request.getURI.toString must startWith("https://api.twitter.com/1.1/statuses/user_timeline.json?")
        respondWith("[]")
      })

      val tweetData = new TwitterApiImpl(authenticationToken, client)
      tweetData.getTweets("testUser") must haveSize(0)
    }

    "get tweets for a given user" >> {
      val client = new MockHttpClient (request => {
        request.getURI.toString must contain("screen_name=testUser")
        respondWith("[{\"text\":\"Test One\"}, {\"text\":\"Test Two\"}]")
      })

      val tweetData = new TwitterApiImpl(authenticationToken, client)
      val tweets = tweetData.getTweets("testUser")
      tweets must haveSize(2)
      tweets(0).property("text") must beEqualTo("Test One")
      tweets(1).property("text") must beEqualTo("Test Two")
    }

    "not set a count by default when getting tweets" >> {
      val client = new MockHttpClient (request => {
        request.getURI.toString must not contain "count="
        respondWith("[]")
      })

      val tweetData = new TwitterApiImpl(authenticationToken, client)
      tweetData.getTweets("testUser") must haveSize(0)
    }

    "set the correct count parameter when getting tweets" >> {
      val client = new MockHttpClient (request => {
        request.getURI.toString must contain("count=99")
        respondWith("[]")
      })

      val tweetData = new TwitterApiImpl(authenticationToken, client)
      tweetData.getTweets("testUser", Some(99)) must haveSize(0)
    }

    "exclude replies by default when getting tweets" >> {
      val client = new MockHttpClient (request => {
        request.getURI.toString must contain("exclude_replies=true")
        respondWith("[]")
      })

      val tweetData = new TwitterApiImpl(authenticationToken, client)
      tweetData.getTweets("testUser") must haveSize(0)
    }

    "allow replies to be obtained when getting tweets" >> {
      val client = new MockHttpClient (request => {
        request.getURI.toString must contain("exclude_replies=false")
        respondWith("[]")
      })

      val tweetData = new TwitterApiImpl(authenticationToken, client)
      tweetData.getTweets("testUser", excludeReplies = false) must haveSize(0)
    }

    "exclude re-tweets by default when getting tweets" >> {
      val client = new MockHttpClient (request => {
        request.getURI.toString must contain("include_rts=false")
        respondWith("[]")
      })

      val tweetData = new TwitterApiImpl(authenticationToken, client)
      tweetData.getTweets("testUser") must haveSize(0)
    }

    "allow re-tweets to be obtained when getting tweets" >> {
      val client = new MockHttpClient (request => {
        request.getURI.toString must contain("include_rts=true")
        respondWith("[]")
      })

      val tweetData = new TwitterApiImpl(authenticationToken, client)
      tweetData.getTweets("testUser", includeRetweets = true) must haveSize(0)
    }

    "trim user details by default when getting tweets" >> {
      val client = new MockHttpClient (request => {
        request.getURI.toString must contain("trim_user=true")
        respondWith("[]")
      })

      val tweetData = new TwitterApiImpl(authenticationToken, client)
      tweetData.getTweets("testUser") must haveSize(0)
    }

    "allow full user details to be obtained when getting tweets" >> {
      val client = new MockHttpClient (request => {
        request.getURI.toString must contain("trim_user=false")
        respondWith("[]")
      })

      val tweetData = new TwitterApiImpl(authenticationToken, client)
      tweetData.getTweets("testUser", trimUser = false) must haveSize(0)
    }

    "not set a max ID by default when getting tweets" >> {
      val client = new MockHttpClient (request => {
        request.getURI.toString must not contain "max_id="
        respondWith("[]")
      })

      val tweetData = new TwitterApiImpl(authenticationToken, client)
      tweetData.getTweets("testUser") must haveSize(0)
    }

    "set the correct max ID parameter when getting tweets" >> {
      val client = new MockHttpClient (request => {
        request.getURI.toString must contain("max_id=12345")
        respondWith("[]")
      })

      val tweetData = new TwitterApiImpl(authenticationToken, client)
      tweetData.getTweets("testUser", maxId = Some(12345)) must haveSize(0)
    }

    "not set a since ID by default when getting tweets" >> {
      val client = new MockHttpClient (request => {
        request.getURI.toString must not contain "since_id="
        respondWith("[]")
      })

      val tweetData = new TwitterApiImpl(authenticationToken, client)
      tweetData.getTweets("testUser") must haveSize(0)
    }

    "set the correct since ID parameter when getting tweets" >> {
      val client = new MockHttpClient (request => {
        request.getURI.toString must contain("since_id=12345")
        respondWith("[]")
      })

      val tweetData = new TwitterApiImpl(authenticationToken, client)
      tweetData.getTweets("testUser", sinceId = Some(12345)) must haveSize(0)
    }
  }

  private def respondWith(entity: String): HttpResponse = {
    val response = new BasicHttpResponse(new BasicStatusLine(new ProtocolVersion("HTTP", 1, 1), 200, ""))
    response.setEntity(new StringEntity(entity))

    response
  }
}


