package com.nine.ventris.twitter

import org.apache.http.{HttpRequest, HttpHost, HttpResponse}
import org.apache.http.client.{ResponseHandler, HttpClient}
import org.apache.http.client.methods.HttpUriRequest
import org.apache.http.conn.ClientConnectionManager
import org.apache.http.params.HttpParams
import org.apache.http.protocol.HttpContext

class MockHttpClient(key: String, secret: String, executeFunction: HttpUriRequest => HttpResponse) extends HttpClient {

  def this(executeFunction: HttpUriRequest => HttpResponse) = this("", "", executeFunction)

  override def execute(request: HttpUriRequest): HttpResponse = executeFunction(request)

  override def getParams: HttpParams = throw new UnsupportedOperationException

  override def getConnectionManager: ClientConnectionManager = throw new UnsupportedOperationException

  override def execute(request: HttpUriRequest, context: HttpContext): HttpResponse = throw new UnsupportedOperationException

  override def execute(target: HttpHost, request: HttpRequest): HttpResponse = throw new UnsupportedOperationException

  override def execute(target: HttpHost, request: HttpRequest, context: HttpContext): HttpResponse = throw new UnsupportedOperationException

  override def execute[T](request: HttpUriRequest, responseHandler: ResponseHandler[_ <: T]): T = throw new UnsupportedOperationException

  override def execute[T](request: HttpUriRequest, responseHandler: ResponseHandler[_ <: T], context: HttpContext): T = throw new UnsupportedOperationException

  override def execute[T](target: HttpHost, request: HttpRequest, responseHandler: ResponseHandler[_ <: T]): T = throw new UnsupportedOperationException

  override def execute[T](target: HttpHost, request: HttpRequest, responseHandler: ResponseHandler[_ <: T], context: HttpContext): T = throw new UnsupportedOperationException
}
