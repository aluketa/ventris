package com.nine.ventris.command

import com.nine.ventris.classification.Classification
import com.nine.ventris.classification.Classification.Classification
import com.nine.ventris.config.{ClassificationConfiguration, VentrisConfiguration}
import com.nine.ventris.persistence.classification.ClassificationDb
import com.nine.ventris.persistence.tweet.{RunInfo, TweetDb}
import com.nine.ventris.twitter.Tweet
import com.nine.ventris.util.Migrator
import io.dropwizard.setup.Bootstrap
import org.joda.time.LocalDate
import org.specs2.mutable.Specification

class ClassifyTweetsSpec extends Specification {
  private val startDate = LocalDate.parse("2014-12-28")
  private val endDate = LocalDate.parse("2014-12-30")

  "The Classify Tweets command" should {
    "migrate the database schema if requested to do so" >> {
      val mockMigrator: MockMigrator = new MockMigrator
      createCommand(mockMigrator).run(new Bootstrap(null), null, config(migrateDb = true))

      mockMigrator.migrated must beTrue
    }

    "not migrate the database schema if requested not to do so" >> {
      val mockMigrator: MockMigrator = new MockMigrator
      createCommand(mockMigrator).run(new Bootstrap(null), null, config(migrateDb = false))

      mockMigrator.migrated must beFalse
    }

    "delete existing classifications for the specified date range" >> {
      val mockClassificationDb = new MockClassificationDb
      createCommand(mockClassificationDb = mockClassificationDb).run(new Bootstrap(null), null, config(migrateDb = false))

      mockClassificationDb.classificationsDeleted must beTrue
    }

    "correctly persist classifications for the given tweets" >> {
      val tweet1 = Tweet(1, "formed of 8 cars instead of 12")
      val tweet2 = Tweet(2, "due to a train failure")
      val mockTweetDb = new MockTweetDb(Seq(tweet1, tweet2))
      val mockClassificationDb = new MockClassificationDb
      createCommand(mockTweetDb = mockTweetDb, mockClassificationDb = mockClassificationDb).run(new Bootstrap(null), null, config(migrateDb = false))

      mockClassificationDb.savedClassifications(tweet1) must beEqualTo(Seq(Classification.ShortFormation))
      mockClassificationDb.savedClassifications(tweet2) must beEqualTo(Seq(Classification.TrainFailure))
    }
  }

  private def createCommand(mockMigrator: MockMigrator = new MockMigrator,
                            mockTweetDb: MockTweetDb = new MockTweetDb,
                            mockClassificationDb: MockClassificationDb = new MockClassificationDb): ClassifyTweets = new ClassifyTweets {
    override protected val migrator: Migrator = mockMigrator
    override protected def configureTweetDb(config: VentrisConfiguration): TweetDb = mockTweetDb
    override protected def configureClassificationDb(bootstrap: Bootstrap[VentrisConfiguration], config: VentrisConfiguration): ClassificationDb = mockClassificationDb
  }

  private def config(migrateDb: Boolean): VentrisConfiguration = {
    val classificationConfig = new ClassificationConfiguration
    classificationConfig.setMigrateDb(migrateDb)
    classificationConfig.setStartDate(startDate)
    classificationConfig.setEndDate(endDate)

    val config = new VentrisConfiguration
    config.setClassificationConfiguration(classificationConfig)

    config
  }

  private class MockMigrator extends Migrator {
    var migrated = false
    override def migrate(bootstrap: Bootstrap[VentrisConfiguration], configuration: VentrisConfiguration): Unit = {
      migrated = true
    }
  }

  private class MockTweetDb(tweets: Seq[Tweet] = Seq()) extends TweetDb {
    override def saveTweet(tweet: Tweet): Unit = {}
    override def getTweets(startDate: LocalDate, endDate: LocalDate): Seq[Tweet] = tweets
    def recordRunInfo(runInfo: RunInfo): Unit = {}
    def lastRunInfo: Option[RunInfo] = None
  }

  private class MockClassificationDb extends ClassificationDb {
    var classificationsDeleted = false
    var savedClassifications = Map[Tweet, Seq[Classification]]()

    override def saveClassifications(tweet: Tweet, classifications: Seq[Classification]): Unit = {
      savedClassifications += (tweet -> classifications)
    }

    override def deleteClassifications(startDate: LocalDate, endDate: LocalDate): Unit = {
      startDate must beEqualTo(ClassifyTweetsSpec.this.startDate)
      endDate must beEqualTo(ClassifyTweetsSpec.this.endDate)

      classificationsDeleted = true
    }
  }
}
