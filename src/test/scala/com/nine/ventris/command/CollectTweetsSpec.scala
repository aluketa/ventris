package com.nine.ventris.command

import com.google.common.util.concurrent.RateLimiter
import com.nine.ventris.config.{TwitterConfiguration, VentrisConfiguration}
import com.nine.ventris.persistence.tweet.{RunInfo, TweetDb}
import com.nine.ventris.twitter.{Tweet, TwitterApi}
import com.nine.ventris.util.Emailer
import org.joda.time.LocalDate
import org.specs2.matcher.MatchResult
import org.specs2.mutable.Specification

import scala.collection.mutable.ListBuffer

class CollectTweetsSpec extends Specification {

  "The Collect Tweets command" should {
    "collect tweets for the given user" >> {
      val mockTweetDb: MockTweetDb = new MockTweetDb
      collectTweetsCommand(new MockTwitterApi(Seq(Tweet(1234))), mockTweetDb).run(null, null, config)

      mockTweetDb.tweets must haveSize(1)
      mockTweetDb.tweets(0) must beEqualTo(Tweet(1234))
    }

    "loop around until all tweets have been retrieved with no since ID specified" >> {
      val mockTweetDb: MockTweetDb = new MockTweetDb
      val mockTwitterApi = new TwitterApi {
        override def getTweets(user: String, count: Option[Int], sinceId: Option[Long], maxId: Option[Long], excludeReplies: Boolean, includeRetweets: Boolean, trimUser: Boolean): Seq[Tweet] = maxId match {
          case None => Seq(Tweet(5), Tweet(4))
          case Some(3) => Seq(Tweet(3), Tweet(2))
          case Some(1) => Seq()
          case _ => throw new RuntimeException(s"Invalid maximum ID passed: $maxId")
        }
      }

      collectTweetsCommand(mockTwitterApi, mockTweetDb).run(null, null, config)
      mockTweetDb.tweets must haveSize(4)
      mockTweetDb.tweets(0) must beEqualTo(Tweet(5))
      mockTweetDb.tweets(1) must beEqualTo(Tweet(4))
      mockTweetDb.tweets(2) must beEqualTo(Tweet(3))
      mockTweetDb.tweets(3) must beEqualTo(Tweet(2))
    }

    "loop around until all tweets have been retrieved using since ID derived from maximum id from tweet db" >> {
      val mockTweetDb: MockTweetDb = new MockTweetDb(lastRunInfo = Some(RunInfo(null, null, 0, 100L)))
      val mockTwitterApi = new TwitterApi {
        override def getTweets(user: String, count: Option[Int], sinceId: Option[Long], maxId: Option[Long], excludeReplies: Boolean, includeRetweets: Boolean, trimUser: Boolean): Seq[Tweet] = {
          sinceId must beSome(101L)
          maxId match {
            case None => Seq(Tweet(101), Tweet(102))
            case Some(100) => Seq()
            case _ => throw new RuntimeException(s"Invalid max ID passed: $sinceId")
          }
        }
      }

      collectTweetsCommand(mockTwitterApi, mockTweetDb).run(null, null, config)
      mockTweetDb.tweets must haveSize(2)
      mockTweetDb.tweets(0) must beEqualTo(Tweet(101))
      mockTweetDb.tweets(1) must beEqualTo(Tweet(102))
    }

    "send an email notification upon successful completion" >> {
      val mockTweetDb: MockTweetDb = new MockTweetDb
      val mockTwitterApi = new TwitterApi {
        override def getTweets(user: String, count: Option[Int], sinceId: Option[Long], maxId: Option[Long], excludeReplies: Boolean, includeRetweets: Boolean, trimUser: Boolean): Seq[Tweet] = maxId match {
          case None => Seq(Tweet(101), Tweet(102))
          case _ => Seq()
        }
      }

      val emailer = new MockEmailer
      collectTweetsCommand(mockTwitterApi, mockTweetDb, emailer).run(null, null, config)

      emailer.emailsSent must haveSize(1)
      emailer.emailsSent must contain(allOf(("Tweet retrieval completed successfully", "CollectTweets command successfully processed 2 tweets.")))
    }

    "send an email notification if a failure occurs" >> {
      val mockTweetDb: MockTweetDb = new MockTweetDb
      val mockTwitterApi = new TwitterApi {
        override def getTweets(user: String, count: Option[Int], sinceId: Option[Long], maxId: Option[Long], excludeReplies: Boolean, includeRetweets: Boolean, trimUser: Boolean): Seq[Tweet] = {
          throw new RuntimeException("Test Exception")
        }
      }

      val emailer = new MockEmailer
      collectTweetsCommand(mockTwitterApi, mockTweetDb, emailer).run(null, null, config)

      emailer.emailsSent must haveSize(1)
      emailer.emailsSent(0)._1 must beEqualTo("Tweet retrieval failed")
      emailer.emailsSent(0)._2 must contain("java.lang.RuntimeException: Test Exception")
    }

    "record run information on successful completion" >> {
      val mockTweetDb: MockTweetDb = new MockTweetDb(maxId = Some(5678))
      collectTweetsCommand(new MockTwitterApi(Seq(Tweet(1234))), mockTweetDb).run(null, null, config)

      mockTweetDb.runDetails must haveSize(1)
      assertRunInfo(mockTweetDb.runDetails(0), sinceId = 0, maxId = 5678)
    }

    "not record run information on failure" >> {
      val mockTweetDb: MockTweetDb = new MockTweetDb
      val mockTwitterApi = new TwitterApi {
        override def getTweets(user: String, count: Option[Int], sinceId: Option[Long], maxId: Option[Long], excludeReplies: Boolean, includeRetweets: Boolean, trimUser: Boolean): Seq[Tweet] = {
          throw new RuntimeException("Test Exception")
        }
      }
      collectTweetsCommand(mockTwitterApi, mockTweetDb).run(null, null, config)

      mockTweetDb.runDetails must haveSize(0)
    }

    "record run information with a non zero since ID" >> {
      val mockTweetDb: MockTweetDb = new MockTweetDb(maxId = Some(5678), lastRunInfo = Some(RunInfo(null, null, 0, 100L)))
      collectTweetsCommand(new MockTwitterApi(Seq(Tweet(1234))), mockTweetDb).run(null, null, config)

      mockTweetDb.runDetails must haveSize(1)
      assertRunInfo(mockTweetDb.runDetails(0), sinceId = 101, maxId = 5678)
    }

    "record run information with a zero since ID and a zero max ID" >> {
      val mockTweetDb: MockTweetDb = new MockTweetDb
      collectTweetsCommand(new MockTwitterApi(Seq()), mockTweetDb).run(null, null, config)

      mockTweetDb.runDetails must haveSize(1)
      assertRunInfo(mockTweetDb.runDetails(0), sinceId = 0, maxId = 0)
    }
  }

  private def assertRunInfo(runInfo: RunInfo, sinceId: Long, maxId: Long): MatchResult[_] = {
    runInfo.startTime.isAfter(runInfo.endTime) must beFalse
    runInfo.sinceId must beEqualTo(sinceId)
    runInfo.maxId must beEqualTo(maxId)
  }

  private def collectTweetsCommand(mockTwitterApi: TwitterApi, mockTweetDb: MockTweetDb, emailer: Emailer = new MockEmailer): CollectTweets = new CollectTweets {
    override protected def configureTwitterApi(config: VentrisConfiguration): TwitterApi = mockTwitterApi
    override protected def configureTweetDb(config: VentrisConfiguration): TweetDb = mockTweetDb
    override protected def configureEmailer(config: VentrisConfiguration): Emailer = emailer
    override protected val rateLimiter = RateLimiter.create(99999)
  }

  private def config: VentrisConfiguration = {
    val twitterConfig = new TwitterConfiguration
    twitterConfig.setTargetUserName("greateranglia")

    val config = new VentrisConfiguration
    config.setTwitterConfiguration(twitterConfig)

    config
  }

  private class MockTwitterApi(tweets: Seq[Tweet]) extends TwitterApi {
    override def getTweets(user: String, count: Option[Int], sinceId: Option[Long], maxId: Option[Long], excludeReplies: Boolean, includeRetweets: Boolean, trimUser: Boolean): Seq[Tweet] = {
      user must beEqualTo("greateranglia")
      maxId match {
        case None => tweets
        case _ => Seq()
      }
    }
  }

  private class MockTweetDb(override val minId: Option[Long] = None, override val maxId: Option[Long] = None, override val lastRunInfo: Option[RunInfo] = None) extends TweetDb {
    val tweets = new ListBuffer[Tweet]()
    val runDetails = new ListBuffer[RunInfo]()

    override def saveTweet(tweet: Tweet): Unit = {
      tweets.append(tweet)
    }

    override def getTweets(startDate: LocalDate, endDate: LocalDate): Seq[Tweet] = Seq()

    override def recordRunInfo(runInfo: RunInfo): Unit = runDetails.append(runInfo)
  }

  private class MockEmailer extends Emailer {
    val emailsSent = ListBuffer[(String, String)]()

    override def sendMail(recipient: String, subject: String, body: String): Unit = {}
    override def sendMail(subject: String, body: String): Unit = emailsSent.append((subject, body))
  }
}